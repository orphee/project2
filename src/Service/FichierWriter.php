<?php

namespace App\Service;

use Doctrine\ORM\Mapping as ORM;

class FichierWriter
{
    private $id;
	private $url;
	private $fileP;
	private $texte;

		public function __construct($url){
		$this->url= $url;
		$this->fileP= fopen($url, 'w');
	}

	public function ecrire($msg){
		fwrite($this->fileP, $msg);
		fclose($this->fileP);
	}
	public function close(){
		fclose($this->fileP);
	}

}
