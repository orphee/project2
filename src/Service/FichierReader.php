<?php

namespace App\Service;



class FichierReader
{
    	public $url;
	public $fileP;
	public $taille;
	public $texte;

	public function __construct($url){
		if(file_exists($url)){
			$this->url= $url;
			$this->fileP= fopen($url,'r');
			$this->taille= filesize($url);
			$this->texte= fread($this->fileP, $this->					taille);
			fclose($this->fileP);
		}else
			$this->texte= 'erreur d\'url';
	}

	public function getTexte(){
		return $this->texte;
	}
}
