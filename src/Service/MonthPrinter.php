<?php
namespace App\Service;
class MonthPrinter {
	private $jour1;
	private $nbrJourCeMois;
	private $nbrJourMoisPrec;
	private $j;

	private $annee;
	private $mois;
	public function __construct($annee, $mois){
		$this->annee= $annee;
		$this->mois= $mois;
		$this->jour1= intval(date("w", mktime(0,0,0,$mois,1,			$annee)));
		$this->nbrJourCeMois= intval(date("t", mktime(0,0,0,			$mois,1,$annee)));
		$this->nbrJourMoisPrec= intval(date("t",mktime				(0,0,0,$mois-1,1,$annee)));
	}

		private function printCelJourSemaine(){
		$tab= array										("Dimanche","Lundi","Mardi","Mercredi",
			"Jeudi","Vendredi","Samedi");
		echo("<tr>");
		foreach($tab as $cel){
			echo("<th>$cel</th>");
		}
		echo("</tr>");
	}
	private function printJourPrec($debut, $fin){
		echo("<tr>");
		$this->j=0;
		for($i=$debut;$i<=$fin;$i++){
			$this->j++;
			echo("<td class=\"precedent\">$i</td>");
			if(($this->j % 7) ==0)
				echo("</tr><tr>");

		}
	}
	private function printJourMois($fin){
		if($this->j %7 ==0)
			echo("<tr>");

		for($i=1; $i<=$fin; $i++){
			if(file_exists("$this->annee-$this->mois-$i.txt")){
				echo("<td class=\"event\"><a href=\"event/reader/$this->annee-$this->mois-$i\">$i</a></td>");
			}else{
				echo("<td>$i</td>");
			}
			$this->j++;
			if(($this->j % 7) ==0)
				echo("</tr><tr>");
			
		}
		echo("</tr>");
	}
	public function afficherMois(){
		echo("<table>");
		$this->printCelJourSemaine();
		if($this->jour1 !== 0){
			$debutMoisPrec= $this->nbrJourMoisPrec - 					($this->jour1-1);
			$this->printJourPrec($debutMoisPrec, $this->				nbrJourMoisPrec);
		}
		$this->printJourMois($this->nbrJourCeMois);
		echo("</table>");
	}
	public function getNbrJour(){
		return $this->nbrJourCeMois;
	}
	public function tradMois(){
		switch($this->mois){
			case "1": return "janvier";
			case "2": return "f�vrier";
			case "3": return "mars";
			case "4": return "avril";
			case "5": return "mai";
			case "6": return "juin";
			case "7": return "juillet";
			case "8": return "aout";
			case "9": return "septembre";
			case "10": return "octobre";
			case "11": return "novembre";
			case "12": return "d�cembre";
		}
	}
	public function getMois(){
		return $this->mois;
	}
	public function getDate(){
		return $this->annee."-".$this->mois;
	}
}
	