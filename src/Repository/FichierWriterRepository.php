<?php

namespace App\Repository;

use App\Entity\FichierWriter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FichierWriter|null find($id, $lockMode = null, $lockVersion = null)
 * @method FichierWriter|null findOneBy(array $criteria, array $orderBy = null)
 * @method FichierWriter[]    findAll()
 * @method FichierWriter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FichierWriterRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FichierWriter::class);
    }

//    /**
//     * @return FichierWriter[] Returns an array of FichierWriter objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FichierWriter
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
