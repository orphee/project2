<?php

namespace App\Repository;

use App\Entity\FichierReader;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FichierReader|null find($id, $lockMode = null, $lockVersion = null)
 * @method FichierReader|null findOneBy(array $criteria, array $orderBy = null)
 * @method FichierReader[]    findAll()
 * @method FichierReader[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FichierReaderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FichierReader::class);
    }

//    /**
//     * @return FichierReader[] Returns an array of FichierReader objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FichierReader
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
