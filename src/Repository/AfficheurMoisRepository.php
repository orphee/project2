<?php

namespace App\Repository;

use App\Entity\AfficheurMois;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AfficheurMois|null find($id, $lockMode = null, $lockVersion = null)
 * @method AfficheurMois|null findOneBy(array $criteria, array $orderBy = null)
 * @method AfficheurMois[]    findAll()
 * @method AfficheurMois[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AfficheurMoisRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AfficheurMois::class);
    }

//    /**
//     * @return AfficheurMois[] Returns an array of AfficheurMois objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AfficheurMois
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
