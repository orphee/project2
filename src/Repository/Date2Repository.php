<?php

namespace App\Repository;

use App\Entity\Date2;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Date2|null find($id, $lockMode = null, $lockVersion = null)
 * @method Date2|null findOneBy(array $criteria, array $orderBy = null)
 * @method Date2[]    findAll()
 * @method Date2[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class Date2Repository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Date2::class);
    }

//    /**
//     * @return Date2[] Returns an array of Date2 objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Date2
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
