<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\MonthPrinter;
use App\Entity\Date;
use App\Service\FichierWriter;
use App\Service\FichierReader;
use App\Entity\Task2;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;

class DateController extends AbstractController
{
    /**
     * @Route("/date", name="date")
     */
    public function index(Request $request)
    {
	$date= new date();
	$form= $this->createFormBuilder($date)
		->add('date', DateType::class)
		->add('send', SubmitType::class, array						('label'=>'create'))
		->getForm();
	$form->handleRequest($request);
	if($form->isSubmitted() && $form->isValid()){
		$annee= $date->getYear();
		$mois= $date->getMonth();
		return $this->redirect("validate/$annee/$mois");
	}else{
	return $this->render('date/index.html.twig', array			('form'=> $form->createView(), 'calendrier'=>null));
	}
 }

	/**
     * @Route("/validate/{annee<\d+>}/{mois<\d+>}", name="validate")
     */
    public function print(Request $request, $annee, $mois)
    {
		$task= new Task2();
		$form2= $this->createFormBuilder($task)
		->add('task', TextType::class)
		->add('day', TextType::class)
		->add('save', SubmitType::class, array						('label'=>'createTask'))
		->getForm();
		$form2->handleRequest($request);
		if($form2->isSubmitted() && $form2->isValid()){
			$jour= $task->getDay();
			$str= $annee."-".$mois."-".$jour;
			$writer= new FichierWriter("$str.txt");
			$writer->ecrire($task->getTask());
			return $this->render('date/success.html.twig', array('year'=>$annee, 'month'=>$mois));
		}else{
		     $printer= new MonthPrinter($annee, $mois);
		     return $this->render('date/print.html.twig', array('form2'=>$form2->createView(), 'calendrier'=>$printer->afficherMois()));
		}
    }

    /**
     * @Route("{texte}/{texte2}/event/reader/{slug}", name="event_reader")
     */
    public function read($slug)
    {
	$tab= explode("-", $slug);
	$reader= new FichierReader($slug.".txt");

	return $this->render('date/event.html.twig', array('event'=>$reader->getTexte(), 'year'=>$tab[0], 'month'=>$tab[1]));
    }


}