<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class LuckyController extends AbstractController
{
    /**
     * @Route("/lucky/{max}", name="lucky")
     */
    public function index($max, Request $request)
    {
	$number= random_int(0, $max);
	$this->addFlash('notice', 'yoyoyo');
        return $this->render('lucky/index.html.twig', [
            'number' => $number
        ]);
    }
}
